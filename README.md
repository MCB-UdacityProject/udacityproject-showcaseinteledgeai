# Realtime Speech Emotion Recognition with Speaker diarisation and then Speech Synthesis for people suffering with Amyotrophic lateral sclerosis (ALS) in a noisy environment. A project showcase for Intel Edge AI Scholarhip Challenge at Udacity

## My Goal:

My goal is to Build a realtime speech emotion recognition with Speaker diarisation and then Speech Synthesis for people suffering with Amyotrophic lateral sclerosis (ALS), also known as motor neurone disease (MND) in a noisy environment that can be deployed at the edge using Intel's OpenVINO toolkit.

## Motivation:

Recently I watched a Youtube video called [Healed through A.I. | The Age of A.I.](https://www.youtube.com/watch?v=V5aZjsWM2wo) that touched me a lot.
So, I thought to myself why not make an App that would allow to reconstruct with a phone for example the person's voice with a clear pronunciation of what the person is talking, for people suffering with Amyotrophic lateral sclerosis (ALS), also known as motor neurone disease (MND) in a noisy environment.
The noisy environment can be for example other person speaking at the same time or for example when the person is talking near a turn on TV, ... This is why in addition to having realtime Speech Emotion Recognition and a Speech Synthesis, we need to have a Speaker diarisation for identifying the person who speak.

## Strategy:

My Strategy is:

1. Doing some research to better understanding speech emotion recognition, Speech Synthesis and Speaker diarisation.
2. Understanding the libraries that can be used and choose one or two of them.
3. Then after choosing one or two libraries.
4. Afterward I have to learn how to use these libraries and do some test.
5. After understanding I will convert first some audio sound into spectrogram pictures. Then I will convert some sound come from videos to into spectrogram pictures. Then after I understand how it all works I'll try to convert real sound in real time into real time spectrogram pictures.
6. The next step will be to build with Librosa library and Pytorch and perhaps other library a training model for realtime speech emotion recognition, then for realtime Speech Synthesis and for realtime Speaker diarisation.
7. The step 7 will be to do lot of test to be sure that everything works fine.
8. The last step will be to convert the model (realtime speech emotion recognition, realtime Speech Synthesis and for realtime Speaker diarisation.) with OpenVino Toolkit and do some test with Intel Neural Compute Stick 2 + raspberry pi 4 and/or with Intel Neural Compute Stick 2 + Jetson Nano.
9. Build an App for phone.

## Software:
I used in the begin Google Colab, jupyter (anaconda) and spyder (anaconda) for all the test code.

## Original Datasets:

The orginal DataSet I used in the begin is from (The Ryerson Audio-Visual Database of Emotional Speech and Song (RAVDESS)):

https://zenodo.org/record/1188976#.XN0fwnUzZhE

## What I have done:

1. I succeeded to install pytorch and Librosa library in google colab and have done some small test code to better understnding Librosa Library, here is the link [Understanding how the librosa library works in google colab](https://gitlab.com/MCB-UdacityProject/speech_emotion_recognition_Project/-/blob/master/Convert_wav_to_spectrogram_png.ipynb)
2. I succeeded to install Kaldi library in google colab, here is the link which show all the steps : [Kaldi Installation in Google colab](https://gitlab.com/MCB-UdacityProject/understanding_kaldi/-/blob/master/Install_kaldi_in_Google_colab.ipynb)
3. I succeeded to code using pyaudio, matplotlib, numpy and scipy a Real-time speech detection with microphone and its spectrogram also in real-time. : [Realtime Spectrogram](https://gitlab.com/MCB-UdacityProject/real-time_speech_detection_with_microphone_and_its_spectrogram)

## Literature Resources:

1. [Amyotrophic lateral sclerosis (ALS)](https://en.wikipedia.org/wiki/Amyotrophic_lateral_sclerosis)
2. [Awesome-pytorch-list](https://github.com/bharathgs/Awesome-pytorch-list)
3. [Awesome-kaldi](https://github.com/YoavRamon/awesome-kaldi)
4. [Awesome-diarization](https://wq2012.github.io/awesome-diarization/)
5. [awesome-speech-recognition-speech-synthesis-papers ](https://github.com/zzw922cn/awesome-speech-recognition-speech-synthesis-papers)
6. [Kaldi for Dummies tutorial](https://kaldi-asr.org/doc/kaldi_for_dummies.html)
7. [Depression Detect](https://github.com/kykiefer/depression-detect)
8. [Literature Review For Speaker Change Detection](https://hedonistrh.github.io/2018-07-09-Literature-Review-for-Speaker-Change-Detection/)
9. [Speech Emotion Analyzer](https://github.com/MITESHPUTHRANNEU/Speech-Emotion-Analyzer)
10. [M. Ravanelli, "Deep Learning for Distant Speech Recognition", PhD Thesis, Unitn 2017.](https://arxiv.org/pdf/1712.06086.pdf)
11. [The PyTorch-Kaldi Speech Recognition Toolkit](https://github.com/mravanelli/pytorch-kaldi)
12. [Series Introduction | Intel Distribution of OpenVINO Toolkit](https://www.youtube.com/watch?v=5eFRM72USa4&list=PLg-UKERBljNzFvoibYx-NV_At4x0UxhDT)
13. [Audio Source Separation](https://www.youtube.com/watch?v=Xr7UOWIniCM&list=PLhA3b2k8R3t0VpYCpCTU2B1h604rvnV4N&index=1)
14. [Deep Learning for Audio Classification](https://www.youtube.com/watch?v=Z7YM-HAz-IY&list=PLhA3b2k8R3t2Ng1WW_7MiXeh1pfQJQi_P)
15. [The Ultimate Guide To Speech Recognition With Python](https://realpython.com/python-speech-recognition/#installing-pyaudio)

